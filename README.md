# pacote1

> Pacote de Exemplo

## Instalação

> Requer PHP 7 e a extensão "mbstring"

```bash
composer require braian/pacote1
```

## API
```php
namespace braian\pacote1;
class Exemplo {
    /**
     * Retorna o nome...
    */
    function nome();
}
```

## Licença

MIT